(require 'generic-x) ;; Used for simple highlighting.

(define-generic-mode
  'joe-mode ;; Name of the mode.
  '("//") ;; Comments start with //, they're java-style.
  '("actor" "understands" "from" "new" "sender" "this") ;; Keywords
  '(("=" . 'font-lock-operator) ;; Operator =
    ("-" . 'font-lock-operator) ;; Also an operator, -
    ("actor\\W+\\(\\w+\\)" 1 'font-lock-type-face) ;; Correctly highlight actor def name
    ("\\(\\w+\\)\\W\\(\\w+\\)\\W*=\\W*" (1 'font-lock-type-face) (2 'font-lock-variable-name-face))
    ("new\\W+\\(\\w+\\)(" 1 'font-lock-type-face)
    ("\\(\\w+\\)<\\(\\w+\\)>\\W+\\([A-Za-z0-9_]+\\)" (1 'font-lock-type-face) (2 'font-lock-type-face) (3 'font-lock-variable-name-face))
    ("this\\.\\([A-Za-z0-9_]+\\)" . font-lock-variable-name-face)
    )
  '("\\.joe$") ;; File extensions to use for.
  nil ;; No other functions to call.
  "A mode for Joe source files."
)
