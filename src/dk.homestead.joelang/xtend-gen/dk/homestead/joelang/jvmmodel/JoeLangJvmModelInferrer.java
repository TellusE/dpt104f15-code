package dk.homestead.joelang.jvmmodel;

import com.google.inject.Inject;
import dk.homestead.joelang.joeLang.ActorDeclaration;
import dk.homestead.joelang.joeLang.JoeLangFile;
import dk.homestead.joelang.joeLang.MethodDeclaration;
import dk.homestead.joelang.joeLang.MethodDefinition;
import dk.homestead.joelang.joeLang.ParameterDeclaration;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmMember;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

/**
 * <p>Infers a JVM model from the source model.</p>
 * 
 * <p>The JVM model should contain all elements that would appear in the Java code
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>
 */
@SuppressWarnings("all")
public class JoeLangJvmModelInferrer extends AbstractModelInferrer {
  /**
   * convenience API to build and initialize JVM types and their members.
   */
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  /**
   * The dispatch method {@code infer} is called for each instance of the
   * given element's type that is contained in a resource.
   * 
   * @param element
   *            the model to create one or more
   *            {@link org.eclipse.xtext.common.types.JvmDeclaredType declared
   *            types} from.
   * @param acceptor
   *            each created
   *            {@link org.eclipse.xtext.common.types.JvmDeclaredType type}
   *            without a container should be passed to the acceptor in order
   *            get attached to the current resource. The acceptor's
   *            {@link IJvmDeclaredTypeAcceptor#accept(org.eclipse.xtext.common.types.JvmDeclaredType)
   *            accept(..)} method takes the constructed empty type for the
   *            pre-indexing phase. This one is further initialized in the
   *            indexing phase using the closure you pass to the returned
   *            {@link org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor.IPostIndexingInitializing#initializeLater(org.eclipse.xtext.xbase.lib.Procedures.Procedure1)
   *            initializeLater(..)}.
   * @param isPreIndexingPhase
   *            whether the method is called in a pre-indexing phase, i.e.
   *            when the global index is not yet fully updated. You must not
   *            rely on linking using the index if isPreIndexingPhase is
   *            <code>true</code>.
   */
  protected void _infer(final JoeLangFile element, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
  }
  
  protected void _infer(final ActorDeclaration actor, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    String _name = actor.getName();
    JvmGenericType _class = this._jvmTypesBuilder.toClass(actor, _name);
    final Procedure1<JvmGenericType> _function = (JvmGenericType it) -> {
      String _documentation = this._jvmTypesBuilder.getDocumentation(actor);
      this._jvmTypesBuilder.setDocumentation(it, _documentation);
      boolean _isHasProtocol = actor.isHasProtocol();
      if (_isHasProtocol) {
        System.out.println("WIP: handle protocol bound to actor.");
      }
      EList<MethodDefinition> _elements = actor.getElements();
      for (final MethodDefinition md : _elements) {
        {
          System.out.println("WIP: Adding a method");
          EList<JvmMember> _members = it.getMembers();
          MethodDeclaration _method = md.getMethod();
          String _name_1 = _method.getName();
          MethodDeclaration _method_1 = md.getMethod();
          JvmTypeReference _returnType = _method_1.getReturnType();
          final Procedure1<JvmOperation> _function_1 = (JvmOperation it_1) -> {
            MethodDeclaration _method_2 = md.getMethod();
            EList<ParameterDeclaration> _parameters = _method_2.getParameters();
            for (final ParameterDeclaration pd : _parameters) {
              EList<JvmFormalParameter> _parameters_1 = it_1.getParameters();
              String _name_2 = pd.getName();
              JvmTypeReference _parameterType = pd.getParameterType();
              JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(md, _name_2, _parameterType);
              this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter);
            }
          };
          JvmOperation _method_2 = this._jvmTypesBuilder.toMethod(md, _name_1, _returnType, _function_1);
          this._jvmTypesBuilder.<JvmOperation>operator_add(_members, _method_2);
        }
      }
    };
    acceptor.<JvmGenericType>accept(_class, _function);
  }
  
  public void infer(final EObject actor, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    if (actor instanceof ActorDeclaration) {
      _infer((ActorDeclaration)actor, acceptor, isPreIndexingPhase);
      return;
    } else if (actor instanceof JoeLangFile) {
      _infer((JoeLangFile)actor, acceptor, isPreIndexingPhase);
      return;
    } else if (actor != null) {
      _infer(actor, acceptor, isPreIndexingPhase);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(actor, acceptor, isPreIndexingPhase).toString());
    }
  }
}
