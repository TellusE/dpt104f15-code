/**
 */
package dk.homestead.joelang.joeLang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Protocol Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link dk.homestead.joelang.joeLang.ProtocolDeclaration#getMessages <em>Messages</em>}</li>
 * </ul>
 * </p>
 *
 * @see dk.homestead.joelang.joeLang.JoeLangPackage#getProtocolDeclaration()
 * @model
 * @generated
 */
public interface ProtocolDeclaration extends JoeLangFile
{
  /**
   * Returns the value of the '<em><b>Messages</b></em>' containment reference list.
   * The list contents are of type {@link dk.homestead.joelang.joeLang.MethodDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Messages</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Messages</em>' containment reference list.
   * @see dk.homestead.joelang.joeLang.JoeLangPackage#getProtocolDeclaration_Messages()
   * @model containment="true"
   * @generated
   */
  EList<MethodDeclaration> getMessages();

} // ProtocolDeclaration
