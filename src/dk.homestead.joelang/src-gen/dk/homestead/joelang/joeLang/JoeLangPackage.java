/**
 */
package dk.homestead.joelang.joeLang;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.homestead.joelang.joeLang.JoeLangFactory
 * @model kind="package"
 * @generated
 */
public interface JoeLangPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "joeLang";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.homestead.dk/joelang/JoeLang";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "joeLang";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  JoeLangPackage eINSTANCE = dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl.init();

  /**
   * The meta object id for the '{@link dk.homestead.joelang.joeLang.impl.JoeLangFileImpl <em>File</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see dk.homestead.joelang.joeLang.impl.JoeLangFileImpl
   * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getJoeLangFile()
   * @generated
   */
  int JOE_LANG_FILE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOE_LANG_FILE__NAME = 0;

  /**
   * The number of structural features of the '<em>File</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JOE_LANG_FILE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link dk.homestead.joelang.joeLang.impl.ProtocolDeclarationImpl <em>Protocol Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see dk.homestead.joelang.joeLang.impl.ProtocolDeclarationImpl
   * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getProtocolDeclaration()
   * @generated
   */
  int PROTOCOL_DECLARATION = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROTOCOL_DECLARATION__NAME = JOE_LANG_FILE__NAME;

  /**
   * The feature id for the '<em><b>Messages</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROTOCOL_DECLARATION__MESSAGES = JOE_LANG_FILE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Protocol Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROTOCOL_DECLARATION_FEATURE_COUNT = JOE_LANG_FILE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link dk.homestead.joelang.joeLang.impl.ActorDeclarationImpl <em>Actor Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see dk.homestead.joelang.joeLang.impl.ActorDeclarationImpl
   * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getActorDeclaration()
   * @generated
   */
  int ACTOR_DECLARATION = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTOR_DECLARATION__NAME = JOE_LANG_FILE__NAME;

  /**
   * The feature id for the '<em><b>Has Protocol</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTOR_DECLARATION__HAS_PROTOCOL = JOE_LANG_FILE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Protocol</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTOR_DECLARATION__PROTOCOL = JOE_LANG_FILE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTOR_DECLARATION__ELEMENTS = JOE_LANG_FILE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Actor Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTOR_DECLARATION_FEATURE_COUNT = JOE_LANG_FILE_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link dk.homestead.joelang.joeLang.impl.MethodDefinitionImpl <em>Method Definition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see dk.homestead.joelang.joeLang.impl.MethodDefinitionImpl
   * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getMethodDefinition()
   * @generated
   */
  int METHOD_DEFINITION = 3;

  /**
   * The feature id for the '<em><b>Method</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_DEFINITION__METHOD = 0;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_DEFINITION__BODY = 1;

  /**
   * The number of structural features of the '<em>Method Definition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_DEFINITION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link dk.homestead.joelang.joeLang.impl.FieldDeclarationImpl <em>Field Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see dk.homestead.joelang.joeLang.impl.FieldDeclarationImpl
   * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getFieldDeclaration()
   * @generated
   */
  int FIELD_DECLARATION = 4;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_DECLARATION__TYPE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_DECLARATION__NAME = 1;

  /**
   * The number of structural features of the '<em>Field Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_DECLARATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link dk.homestead.joelang.joeLang.impl.MethodDeclarationImpl <em>Method Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see dk.homestead.joelang.joeLang.impl.MethodDeclarationImpl
   * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getMethodDeclaration()
   * @generated
   */
  int METHOD_DECLARATION = 5;

  /**
   * The feature id for the '<em><b>Return Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_DECLARATION__RETURN_TYPE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_DECLARATION__NAME = 1;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_DECLARATION__PARAMETERS = 2;

  /**
   * The number of structural features of the '<em>Method Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_DECLARATION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link dk.homestead.joelang.joeLang.impl.ParameterDeclarationImpl <em>Parameter Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see dk.homestead.joelang.joeLang.impl.ParameterDeclarationImpl
   * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getParameterDeclaration()
   * @generated
   */
  int PARAMETER_DECLARATION = 6;

  /**
   * The feature id for the '<em><b>Parameter Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_DECLARATION__PARAMETER_TYPE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_DECLARATION__NAME = 1;

  /**
   * The number of structural features of the '<em>Parameter Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PARAMETER_DECLARATION_FEATURE_COUNT = 2;


  /**
   * Returns the meta object for class '{@link dk.homestead.joelang.joeLang.JoeLangFile <em>File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>File</em>'.
   * @see dk.homestead.joelang.joeLang.JoeLangFile
   * @generated
   */
  EClass getJoeLangFile();

  /**
   * Returns the meta object for the attribute '{@link dk.homestead.joelang.joeLang.JoeLangFile#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see dk.homestead.joelang.joeLang.JoeLangFile#getName()
   * @see #getJoeLangFile()
   * @generated
   */
  EAttribute getJoeLangFile_Name();

  /**
   * Returns the meta object for class '{@link dk.homestead.joelang.joeLang.ProtocolDeclaration <em>Protocol Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Protocol Declaration</em>'.
   * @see dk.homestead.joelang.joeLang.ProtocolDeclaration
   * @generated
   */
  EClass getProtocolDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link dk.homestead.joelang.joeLang.ProtocolDeclaration#getMessages <em>Messages</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Messages</em>'.
   * @see dk.homestead.joelang.joeLang.ProtocolDeclaration#getMessages()
   * @see #getProtocolDeclaration()
   * @generated
   */
  EReference getProtocolDeclaration_Messages();

  /**
   * Returns the meta object for class '{@link dk.homestead.joelang.joeLang.ActorDeclaration <em>Actor Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Actor Declaration</em>'.
   * @see dk.homestead.joelang.joeLang.ActorDeclaration
   * @generated
   */
  EClass getActorDeclaration();

  /**
   * Returns the meta object for the attribute '{@link dk.homestead.joelang.joeLang.ActorDeclaration#isHasProtocol <em>Has Protocol</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Has Protocol</em>'.
   * @see dk.homestead.joelang.joeLang.ActorDeclaration#isHasProtocol()
   * @see #getActorDeclaration()
   * @generated
   */
  EAttribute getActorDeclaration_HasProtocol();

  /**
   * Returns the meta object for the attribute '{@link dk.homestead.joelang.joeLang.ActorDeclaration#getProtocol <em>Protocol</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Protocol</em>'.
   * @see dk.homestead.joelang.joeLang.ActorDeclaration#getProtocol()
   * @see #getActorDeclaration()
   * @generated
   */
  EAttribute getActorDeclaration_Protocol();

  /**
   * Returns the meta object for the containment reference list '{@link dk.homestead.joelang.joeLang.ActorDeclaration#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see dk.homestead.joelang.joeLang.ActorDeclaration#getElements()
   * @see #getActorDeclaration()
   * @generated
   */
  EReference getActorDeclaration_Elements();

  /**
   * Returns the meta object for class '{@link dk.homestead.joelang.joeLang.MethodDefinition <em>Method Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Method Definition</em>'.
   * @see dk.homestead.joelang.joeLang.MethodDefinition
   * @generated
   */
  EClass getMethodDefinition();

  /**
   * Returns the meta object for the containment reference '{@link dk.homestead.joelang.joeLang.MethodDefinition#getMethod <em>Method</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Method</em>'.
   * @see dk.homestead.joelang.joeLang.MethodDefinition#getMethod()
   * @see #getMethodDefinition()
   * @generated
   */
  EReference getMethodDefinition_Method();

  /**
   * Returns the meta object for the containment reference '{@link dk.homestead.joelang.joeLang.MethodDefinition#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see dk.homestead.joelang.joeLang.MethodDefinition#getBody()
   * @see #getMethodDefinition()
   * @generated
   */
  EReference getMethodDefinition_Body();

  /**
   * Returns the meta object for class '{@link dk.homestead.joelang.joeLang.FieldDeclaration <em>Field Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Declaration</em>'.
   * @see dk.homestead.joelang.joeLang.FieldDeclaration
   * @generated
   */
  EClass getFieldDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link dk.homestead.joelang.joeLang.FieldDeclaration#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see dk.homestead.joelang.joeLang.FieldDeclaration#getType()
   * @see #getFieldDeclaration()
   * @generated
   */
  EReference getFieldDeclaration_Type();

  /**
   * Returns the meta object for the attribute '{@link dk.homestead.joelang.joeLang.FieldDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see dk.homestead.joelang.joeLang.FieldDeclaration#getName()
   * @see #getFieldDeclaration()
   * @generated
   */
  EAttribute getFieldDeclaration_Name();

  /**
   * Returns the meta object for class '{@link dk.homestead.joelang.joeLang.MethodDeclaration <em>Method Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Method Declaration</em>'.
   * @see dk.homestead.joelang.joeLang.MethodDeclaration
   * @generated
   */
  EClass getMethodDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link dk.homestead.joelang.joeLang.MethodDeclaration#getReturnType <em>Return Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Return Type</em>'.
   * @see dk.homestead.joelang.joeLang.MethodDeclaration#getReturnType()
   * @see #getMethodDeclaration()
   * @generated
   */
  EReference getMethodDeclaration_ReturnType();

  /**
   * Returns the meta object for the attribute '{@link dk.homestead.joelang.joeLang.MethodDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see dk.homestead.joelang.joeLang.MethodDeclaration#getName()
   * @see #getMethodDeclaration()
   * @generated
   */
  EAttribute getMethodDeclaration_Name();

  /**
   * Returns the meta object for the containment reference list '{@link dk.homestead.joelang.joeLang.MethodDeclaration#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see dk.homestead.joelang.joeLang.MethodDeclaration#getParameters()
   * @see #getMethodDeclaration()
   * @generated
   */
  EReference getMethodDeclaration_Parameters();

  /**
   * Returns the meta object for class '{@link dk.homestead.joelang.joeLang.ParameterDeclaration <em>Parameter Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Parameter Declaration</em>'.
   * @see dk.homestead.joelang.joeLang.ParameterDeclaration
   * @generated
   */
  EClass getParameterDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link dk.homestead.joelang.joeLang.ParameterDeclaration#getParameterType <em>Parameter Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Parameter Type</em>'.
   * @see dk.homestead.joelang.joeLang.ParameterDeclaration#getParameterType()
   * @see #getParameterDeclaration()
   * @generated
   */
  EReference getParameterDeclaration_ParameterType();

  /**
   * Returns the meta object for the attribute '{@link dk.homestead.joelang.joeLang.ParameterDeclaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see dk.homestead.joelang.joeLang.ParameterDeclaration#getName()
   * @see #getParameterDeclaration()
   * @generated
   */
  EAttribute getParameterDeclaration_Name();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  JoeLangFactory getJoeLangFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link dk.homestead.joelang.joeLang.impl.JoeLangFileImpl <em>File</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see dk.homestead.joelang.joeLang.impl.JoeLangFileImpl
     * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getJoeLangFile()
     * @generated
     */
    EClass JOE_LANG_FILE = eINSTANCE.getJoeLangFile();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute JOE_LANG_FILE__NAME = eINSTANCE.getJoeLangFile_Name();

    /**
     * The meta object literal for the '{@link dk.homestead.joelang.joeLang.impl.ProtocolDeclarationImpl <em>Protocol Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see dk.homestead.joelang.joeLang.impl.ProtocolDeclarationImpl
     * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getProtocolDeclaration()
     * @generated
     */
    EClass PROTOCOL_DECLARATION = eINSTANCE.getProtocolDeclaration();

    /**
     * The meta object literal for the '<em><b>Messages</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROTOCOL_DECLARATION__MESSAGES = eINSTANCE.getProtocolDeclaration_Messages();

    /**
     * The meta object literal for the '{@link dk.homestead.joelang.joeLang.impl.ActorDeclarationImpl <em>Actor Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see dk.homestead.joelang.joeLang.impl.ActorDeclarationImpl
     * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getActorDeclaration()
     * @generated
     */
    EClass ACTOR_DECLARATION = eINSTANCE.getActorDeclaration();

    /**
     * The meta object literal for the '<em><b>Has Protocol</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTOR_DECLARATION__HAS_PROTOCOL = eINSTANCE.getActorDeclaration_HasProtocol();

    /**
     * The meta object literal for the '<em><b>Protocol</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTOR_DECLARATION__PROTOCOL = eINSTANCE.getActorDeclaration_Protocol();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTOR_DECLARATION__ELEMENTS = eINSTANCE.getActorDeclaration_Elements();

    /**
     * The meta object literal for the '{@link dk.homestead.joelang.joeLang.impl.MethodDefinitionImpl <em>Method Definition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see dk.homestead.joelang.joeLang.impl.MethodDefinitionImpl
     * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getMethodDefinition()
     * @generated
     */
    EClass METHOD_DEFINITION = eINSTANCE.getMethodDefinition();

    /**
     * The meta object literal for the '<em><b>Method</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD_DEFINITION__METHOD = eINSTANCE.getMethodDefinition_Method();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD_DEFINITION__BODY = eINSTANCE.getMethodDefinition_Body();

    /**
     * The meta object literal for the '{@link dk.homestead.joelang.joeLang.impl.FieldDeclarationImpl <em>Field Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see dk.homestead.joelang.joeLang.impl.FieldDeclarationImpl
     * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getFieldDeclaration()
     * @generated
     */
    EClass FIELD_DECLARATION = eINSTANCE.getFieldDeclaration();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FIELD_DECLARATION__TYPE = eINSTANCE.getFieldDeclaration_Type();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FIELD_DECLARATION__NAME = eINSTANCE.getFieldDeclaration_Name();

    /**
     * The meta object literal for the '{@link dk.homestead.joelang.joeLang.impl.MethodDeclarationImpl <em>Method Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see dk.homestead.joelang.joeLang.impl.MethodDeclarationImpl
     * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getMethodDeclaration()
     * @generated
     */
    EClass METHOD_DECLARATION = eINSTANCE.getMethodDeclaration();

    /**
     * The meta object literal for the '<em><b>Return Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD_DECLARATION__RETURN_TYPE = eINSTANCE.getMethodDeclaration_ReturnType();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_DECLARATION__NAME = eINSTANCE.getMethodDeclaration_Name();

    /**
     * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference METHOD_DECLARATION__PARAMETERS = eINSTANCE.getMethodDeclaration_Parameters();

    /**
     * The meta object literal for the '{@link dk.homestead.joelang.joeLang.impl.ParameterDeclarationImpl <em>Parameter Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see dk.homestead.joelang.joeLang.impl.ParameterDeclarationImpl
     * @see dk.homestead.joelang.joeLang.impl.JoeLangPackageImpl#getParameterDeclaration()
     * @generated
     */
    EClass PARAMETER_DECLARATION = eINSTANCE.getParameterDeclaration();

    /**
     * The meta object literal for the '<em><b>Parameter Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PARAMETER_DECLARATION__PARAMETER_TYPE = eINSTANCE.getParameterDeclaration_ParameterType();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PARAMETER_DECLARATION__NAME = eINSTANCE.getParameterDeclaration_Name();

  }

} //JoeLangPackage
