/**
 */
package dk.homestead.joelang.joeLang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link dk.homestead.joelang.joeLang.ActorDeclaration#isHasProtocol <em>Has Protocol</em>}</li>
 *   <li>{@link dk.homestead.joelang.joeLang.ActorDeclaration#getProtocol <em>Protocol</em>}</li>
 *   <li>{@link dk.homestead.joelang.joeLang.ActorDeclaration#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see dk.homestead.joelang.joeLang.JoeLangPackage#getActorDeclaration()
 * @model
 * @generated
 */
public interface ActorDeclaration extends JoeLangFile
{
  /**
   * Returns the value of the '<em><b>Has Protocol</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Has Protocol</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Has Protocol</em>' attribute.
   * @see #setHasProtocol(boolean)
   * @see dk.homestead.joelang.joeLang.JoeLangPackage#getActorDeclaration_HasProtocol()
   * @model
   * @generated
   */
  boolean isHasProtocol();

  /**
   * Sets the value of the '{@link dk.homestead.joelang.joeLang.ActorDeclaration#isHasProtocol <em>Has Protocol</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Has Protocol</em>' attribute.
   * @see #isHasProtocol()
   * @generated
   */
  void setHasProtocol(boolean value);

  /**
   * Returns the value of the '<em><b>Protocol</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Protocol</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Protocol</em>' attribute.
   * @see #setProtocol(String)
   * @see dk.homestead.joelang.joeLang.JoeLangPackage#getActorDeclaration_Protocol()
   * @model
   * @generated
   */
  String getProtocol();

  /**
   * Sets the value of the '{@link dk.homestead.joelang.joeLang.ActorDeclaration#getProtocol <em>Protocol</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Protocol</em>' attribute.
   * @see #getProtocol()
   * @generated
   */
  void setProtocol(String value);

  /**
   * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
   * The list contents are of type {@link dk.homestead.joelang.joeLang.MethodDefinition}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elements</em>' containment reference list.
   * @see dk.homestead.joelang.joeLang.JoeLangPackage#getActorDeclaration_Elements()
   * @model containment="true"
   * @generated
   */
  EList<MethodDefinition> getElements();

} // ActorDeclaration
