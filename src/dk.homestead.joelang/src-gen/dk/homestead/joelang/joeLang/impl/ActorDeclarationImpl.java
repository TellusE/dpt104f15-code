/**
 */
package dk.homestead.joelang.joeLang.impl;

import dk.homestead.joelang.joeLang.ActorDeclaration;
import dk.homestead.joelang.joeLang.JoeLangPackage;
import dk.homestead.joelang.joeLang.MethodDefinition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actor Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link dk.homestead.joelang.joeLang.impl.ActorDeclarationImpl#isHasProtocol <em>Has Protocol</em>}</li>
 *   <li>{@link dk.homestead.joelang.joeLang.impl.ActorDeclarationImpl#getProtocol <em>Protocol</em>}</li>
 *   <li>{@link dk.homestead.joelang.joeLang.impl.ActorDeclarationImpl#getElements <em>Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActorDeclarationImpl extends JoeLangFileImpl implements ActorDeclaration
{
  /**
   * The default value of the '{@link #isHasProtocol() <em>Has Protocol</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isHasProtocol()
   * @generated
   * @ordered
   */
  protected static final boolean HAS_PROTOCOL_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isHasProtocol() <em>Has Protocol</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isHasProtocol()
   * @generated
   * @ordered
   */
  protected boolean hasProtocol = HAS_PROTOCOL_EDEFAULT;

  /**
   * The default value of the '{@link #getProtocol() <em>Protocol</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProtocol()
   * @generated
   * @ordered
   */
  protected static final String PROTOCOL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getProtocol() <em>Protocol</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProtocol()
   * @generated
   * @ordered
   */
  protected String protocol = PROTOCOL_EDEFAULT;

  /**
   * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElements()
   * @generated
   * @ordered
   */
  protected EList<MethodDefinition> elements;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ActorDeclarationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return JoeLangPackage.Literals.ACTOR_DECLARATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isHasProtocol()
  {
    return hasProtocol;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHasProtocol(boolean newHasProtocol)
  {
    boolean oldHasProtocol = hasProtocol;
    hasProtocol = newHasProtocol;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JoeLangPackage.ACTOR_DECLARATION__HAS_PROTOCOL, oldHasProtocol, hasProtocol));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getProtocol()
  {
    return protocol;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProtocol(String newProtocol)
  {
    String oldProtocol = protocol;
    protocol = newProtocol;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, JoeLangPackage.ACTOR_DECLARATION__PROTOCOL, oldProtocol, protocol));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<MethodDefinition> getElements()
  {
    if (elements == null)
    {
      elements = new EObjectContainmentEList<MethodDefinition>(MethodDefinition.class, this, JoeLangPackage.ACTOR_DECLARATION__ELEMENTS);
    }
    return elements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case JoeLangPackage.ACTOR_DECLARATION__ELEMENTS:
        return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case JoeLangPackage.ACTOR_DECLARATION__HAS_PROTOCOL:
        return isHasProtocol();
      case JoeLangPackage.ACTOR_DECLARATION__PROTOCOL:
        return getProtocol();
      case JoeLangPackage.ACTOR_DECLARATION__ELEMENTS:
        return getElements();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case JoeLangPackage.ACTOR_DECLARATION__HAS_PROTOCOL:
        setHasProtocol((Boolean)newValue);
        return;
      case JoeLangPackage.ACTOR_DECLARATION__PROTOCOL:
        setProtocol((String)newValue);
        return;
      case JoeLangPackage.ACTOR_DECLARATION__ELEMENTS:
        getElements().clear();
        getElements().addAll((Collection<? extends MethodDefinition>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case JoeLangPackage.ACTOR_DECLARATION__HAS_PROTOCOL:
        setHasProtocol(HAS_PROTOCOL_EDEFAULT);
        return;
      case JoeLangPackage.ACTOR_DECLARATION__PROTOCOL:
        setProtocol(PROTOCOL_EDEFAULT);
        return;
      case JoeLangPackage.ACTOR_DECLARATION__ELEMENTS:
        getElements().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case JoeLangPackage.ACTOR_DECLARATION__HAS_PROTOCOL:
        return hasProtocol != HAS_PROTOCOL_EDEFAULT;
      case JoeLangPackage.ACTOR_DECLARATION__PROTOCOL:
        return PROTOCOL_EDEFAULT == null ? protocol != null : !PROTOCOL_EDEFAULT.equals(protocol);
      case JoeLangPackage.ACTOR_DECLARATION__ELEMENTS:
        return elements != null && !elements.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (hasProtocol: ");
    result.append(hasProtocol);
    result.append(", protocol: ");
    result.append(protocol);
    result.append(')');
    return result.toString();
  }

} //ActorDeclarationImpl
