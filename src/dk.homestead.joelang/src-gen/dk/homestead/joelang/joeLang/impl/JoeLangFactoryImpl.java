/**
 */
package dk.homestead.joelang.joeLang.impl;

import dk.homestead.joelang.joeLang.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JoeLangFactoryImpl extends EFactoryImpl implements JoeLangFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static JoeLangFactory init()
  {
    try
    {
      JoeLangFactory theJoeLangFactory = (JoeLangFactory)EPackage.Registry.INSTANCE.getEFactory(JoeLangPackage.eNS_URI);
      if (theJoeLangFactory != null)
      {
        return theJoeLangFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new JoeLangFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JoeLangFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case JoeLangPackage.JOE_LANG_FILE: return createJoeLangFile();
      case JoeLangPackage.PROTOCOL_DECLARATION: return createProtocolDeclaration();
      case JoeLangPackage.ACTOR_DECLARATION: return createActorDeclaration();
      case JoeLangPackage.METHOD_DEFINITION: return createMethodDefinition();
      case JoeLangPackage.FIELD_DECLARATION: return createFieldDeclaration();
      case JoeLangPackage.METHOD_DECLARATION: return createMethodDeclaration();
      case JoeLangPackage.PARAMETER_DECLARATION: return createParameterDeclaration();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JoeLangFile createJoeLangFile()
  {
    JoeLangFileImpl joeLangFile = new JoeLangFileImpl();
    return joeLangFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProtocolDeclaration createProtocolDeclaration()
  {
    ProtocolDeclarationImpl protocolDeclaration = new ProtocolDeclarationImpl();
    return protocolDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActorDeclaration createActorDeclaration()
  {
    ActorDeclarationImpl actorDeclaration = new ActorDeclarationImpl();
    return actorDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodDefinition createMethodDefinition()
  {
    MethodDefinitionImpl methodDefinition = new MethodDefinitionImpl();
    return methodDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldDeclaration createFieldDeclaration()
  {
    FieldDeclarationImpl fieldDeclaration = new FieldDeclarationImpl();
    return fieldDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodDeclaration createMethodDeclaration()
  {
    MethodDeclarationImpl methodDeclaration = new MethodDeclarationImpl();
    return methodDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParameterDeclaration createParameterDeclaration()
  {
    ParameterDeclarationImpl parameterDeclaration = new ParameterDeclarationImpl();
    return parameterDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JoeLangPackage getJoeLangPackage()
  {
    return (JoeLangPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static JoeLangPackage getPackage()
  {
    return JoeLangPackage.eINSTANCE;
  }

} //JoeLangFactoryImpl
